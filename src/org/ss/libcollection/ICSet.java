package org.ss.libcollection;

import org.ss.libcollection.exception.CSetIndexNotFoundException;
import org.ss.libcollection.exception.CSetItemAlreadyExistException;
import org.ss.libcollection.exception.CSetMaxSizeReachedException;

public interface ICSet {
	
	public static int DEFAULT_MAX_SIZE = 5;
	
	public void add( Object o ) throws CSetMaxSizeReachedException, CSetItemAlreadyExistException;
	
	public boolean isEmpty();
	
	public boolean contains( Object o );
	
	public Object get(int index) throws CSetIndexNotFoundException;
	
	public Object remove(int index) throws CSetIndexNotFoundException;
	
	public ICSet copy();
	
	public ICSet merge( ICSet set );
	
	public int getSize();
	
	public int getMaxSize();
}
