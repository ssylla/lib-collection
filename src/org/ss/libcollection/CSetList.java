package org.ss.libcollection;

import org.ss.libcollection.exception.CSetIndexNotFoundException;
import org.ss.libcollection.exception.CSetItemAlreadyExistException;
import org.ss.libcollection.exception.CSetMaxSizeReachedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSetList implements ICSet {
	
	private List data;
	
	public CSetList() {
		data = new ArrayList();
	}
	
	public CSetList( Object[] data ) {
		this.data = Arrays.asList( data );
	}
	
	public CSetList( List data ) {
		this.data = data;
	}
	
	@Override
	public void add( Object o ) throws CSetMaxSizeReachedException, CSetItemAlreadyExistException {
		if ( data.contains( o ) ) throw new CSetItemAlreadyExistException( this, o );
		data.add( o );
	}
	
	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}
	
	@Override
	public boolean contains( Object o ) {
		return data.contains( o );
	}
	
	@Override
	public Object get( int index ) throws CSetIndexNotFoundException {
		return data.get( index );
	}
	
	@Override
	public Object remove( int index ) throws CSetIndexNotFoundException {
		return data.remove( index );
	}
	
	@Override
	public ICSet copy() {
		return new CSetList( data );
	}
	
	@Override
	public ICSet merge( ICSet set ) {
		ICSet tempSet = new CSetList( data );
		for ( int i = 0; i < set.getSize(); ++i ) {
			try {
				tempSet.add( set.get( i ) );
			} catch ( CSetIndexNotFoundException | CSetMaxSizeReachedException | CSetItemAlreadyExistException e ) {
			
			}
		}
		return tempSet;
	}
	
	@Override
	public int getSize() {
		return data.size();
	}
	
	@Override
	public int getMaxSize() {
		return Integer.MAX_VALUE;
	}
}
