package org.ss.libcollection.exception;

import org.ss.libcollection.ICSet;

public abstract class CSetException extends Exception {
	
	ICSet set;
	
	protected CSetException() {	}
	
	public CSetException( ICSet set ) {
		this.set = set;
	}
}
