package org.ss.libcollection.exception;

import org.ss.libcollection.ICSet;

public class CSetItemAlreadyExistException extends CSetException {
	
	Object duplicate;
	
	public CSetItemAlreadyExistException( ICSet set, Object o ) {
		super( set );
		duplicate = o;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder( "L'élément que vous essayez d'ajouter existe déjà dans cet ensemble." );
		sb.append( duplicate.toString() );
		return sb.toString();
	}
}
