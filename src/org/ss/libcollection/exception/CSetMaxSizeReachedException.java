package org.ss.libcollection.exception;

import org.ss.libcollection.ICSet;

public class CSetMaxSizeReachedException extends CSetException {
	
	public CSetMaxSizeReachedException( ICSet set ) {
		this.set = set;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder( "La taille maximale des éléments de l'ensemble est atteinte..." );
		sb.append( "Le nombre d'éléments ne peut pas dépasser : " ).append( set.getMaxSize() );
		return sb.toString();
	}
}
