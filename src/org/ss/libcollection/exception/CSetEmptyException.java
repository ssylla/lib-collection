package org.ss.libcollection.exception;

import org.ss.libcollection.ICSet;

public class CSetEmptyException extends CSetException {
	
	public CSetEmptyException( ICSet set ) {
		this.set = set;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder( "L'ensemble est vide !!!" );
		sb.append( "Commencez par ajouter des éléments - Vous pouvez en ajouter jusqu'à :" + set.getMaxSize() );
		return sb.toString();
	}
}
