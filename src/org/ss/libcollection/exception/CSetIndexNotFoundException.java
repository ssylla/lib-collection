package org.ss.libcollection.exception;

import org.ss.libcollection.ICSet;

public class CSetIndexNotFoundException extends CSetException {
	
	public CSetIndexNotFoundException( ICSet set ) {
		this.set = set;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder( "L'index fourni n'est pas présent dans cet ensemble !!!" );
		sb.append( "Les index doivent être compris entre 0 et :" + (set.getSize() - 1) );
		return sb.toString();
	}
}
