package org.ss.libcollection;

public interface ICStack {
	
	void push( Object o );
	
	Object pop();
	
	Object peek();
	
	int size();
	
	int getMaxSize();
}
