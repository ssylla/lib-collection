package org.ss.libcollection;

import org.ss.libcollection.exception.CSetIndexNotFoundException;
import org.ss.libcollection.exception.CSetItemAlreadyExistException;
import org.ss.libcollection.exception.CSetMaxSizeReachedException;

public class CSetArray implements ICSet {
	
	private Object[] data;
	private int lastIndex = -1;
	
	public CSetArray() {
		data = new Object[DEFAULT_MAX_SIZE];
	}
	
	public CSetArray( int size ) {
		data = new Object[size];
	}
	
	public CSetArray( Object[] data, int lastIndex ) {
		this.data = data;
		this.lastIndex = lastIndex;
	}
	
	public CSetArray( CSetArray set ) {
		this.data = set.data;
		this.lastIndex = set.lastIndex;
	}
	
	@Override
	public void add( Object o ) throws CSetMaxSizeReachedException, CSetItemAlreadyExistException {
		if ( lastIndex >= data.length -1 ) throw new CSetMaxSizeReachedException( this );
		boolean doAction = true;
		for ( int i = 0; i <= lastIndex; ++i ) {
			if ( data[i].equals( o ) ) {
				doAction = false;
				break;
			}
		}
		if ( doAction ) {
			data[++lastIndex] = o;
		} else throw new CSetItemAlreadyExistException( this, o );
	}
	
	@Override
	public boolean isEmpty() {
		return -1 == lastIndex;
	}
	
	@Override
	public boolean contains( Object o ) {
		boolean found = false;
		for ( Object oTmp : data ) {
			if ( oTmp.equals( o ) ) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	@Override
	public ICSet copy() {
		CSetArray set = new CSetArray( this );
		return set;
	}
	
	@Override
	public Object get( int index ) throws CSetIndexNotFoundException {
		Object o = null;
		try {
			o = data[index];
		} catch ( ArrayIndexOutOfBoundsException e ) {
			throw new CSetIndexNotFoundException( this );
		}
		return o;
	}
	
	@Override
	public Object remove( int index ) throws CSetIndexNotFoundException {
		Object o = get( index );
		for ( int i = index; i < data.length-1; ++i ) {
			data[i] = data[i+1];
		}
		lastIndex--;
		return o;
	}
	
	@Override
	public ICSet merge( ICSet set ) {
		ICSet setTemp = new CSetArray( data.length + set.getMaxSize() );
		for ( int i = 0; i <= lastIndex; ++i ) {
			try {
				setTemp.add( data[i] );
			} catch ( CSetMaxSizeReachedException|CSetItemAlreadyExistException e ) {}
		}
		for ( int i = 0; i <= set.getSize(); ++i ) {
			try {
				setTemp.add( set.get( i ) );
			} catch ( CSetIndexNotFoundException|CSetMaxSizeReachedException|CSetItemAlreadyExistException e ) {}
		}
		return setTemp;
	}
	
	@Override
	public int getSize() {
		return lastIndex+1;
	}
	
	@Override
	public int getMaxSize() {
		return data.length;
	}
}
